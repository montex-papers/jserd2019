\documentclass{article}
\usepackage[utf8]{inputenc}

\begin{document}

\title{Response Letter}
\maketitle

Dear Editors and Reviewers,

\vspace{1cm}

We would like to thank you for the effort on reviewing the manuscript and providing valuable comments to improve our work.

As an extended version of the CIbSE 2019 paper, the content has been modified in different ways with respect to the initial version. In particular, we provide the following additional contributions:
i) we expand the analysis of the state of the art, discussing the limitations of related work, and justifying our choice of the MAPE-K architecture as a reference structure and behavior for the RoCS framework (Section 3);
ii) we improve the description of the Knowledge component, which was only briefly introduced in the conference version (Section 4.3).
iv) we improve the evaluation by reporting on two new experiments, involving robots with different physical structures and applications with different tasks (Section 6), and
iii) we clarify the relationship of our proposal to the very popular  ROS (Robotics Operating System) middleware (Section 7). 

Through a careful reading of the reviewers' comments, we worked on this version of the manuscript by ensuring proofreading, clarifying ideas, and giving additional details regarding our results. 

We understand that this extended version satisfies the requirement of providing more than 30\% of new material, constituting a good extension of the original work. For additional evidence on that, we also attach two files presenting: i) a diff from the CIbSE version and ii) a diff of the JSERD original submission. We attach below our responses for the reviewer's comments.

\vspace{1cm}
\textbf{Authors}:\\
\par Leonardo Ramos, Gabriel Lisbôa Guimarães Divino, Guilherme Cano Lopes: \{leo.o.rms,gabriel.lg.divino,gui.c.lopes\}@gmail.com\\

Breno Bernard Nicolau de França, Leonardo Montecchi, Esther Luna Colombini: \{breno,leonardo,esther\}@ic.unicamp.br

\vspace{1cm}

---------------------------------------------------------------------------------------------------

\textbf{Reviewer A}

\textit{``I consider that this article has been extended and improved in an appropriate manner with respect to the version presented in CIbSE. In particular, the evaluation of the proposal was carried out in a more complete and detailed way.  In this version of the paper, the authors performed two additional experiments exploring the reuse capability of the proposed framework by changing robot models and their plans. Additionally, a section was added where the connection between this proposal and the well-known Robot Operating System (ROS) was  explained''}

\vspace{0.5cm}

\underline{Response}: We thank the reviewer for emphasizing the additional contributions. Providing additional experiments and describing the connection with ROS were the main concerns reported by the CIbSE reviewers. We thus worked on overcoming them in the journal version.

---------------------------------------------------------------------------------------------------

\textbf{Reviewer B}

\textit{``The paper is an extension of the CIbSE 2019 paper "The RoCS Framework to Support the Development of Autonomous Robots", which presents a framework to guide developers of software for robotic systems on reusing existing modules. The framework is based on the MAPE-K architecture and it was tested under simulated robotics scenarios.}

\textit{Points in Favor (same as the conference version)}:
\begin{itemize}
    \item \textit{Well written paper, properly organized.}
    \item \textit{Good review of related work, motivation clearly explained.}
    \item \textit{Source code is available, video of proof of concept is available.}
\end{itemize}

\textit{Points Against:}
\begin{itemize}
    \item \textit{Although extended in this version of the paper, evaluation is still just a proof of concept.}
    \item \textit{Extension is thin. A lot of it on related work.''}
\end{itemize}

\bigskip
\underline{Response}: Thank you for the comments. We do understand the provided experiments as proof of concept as well. However, even as such, they show RoCS can achieve two important capabilities expected from a robotics framework: i) reuse of robot model (or physical structure) for performing different behaviors or tasks, and ii) reuse of robot behavior or plans to be performed by different robot models (or physical structure). For that, our experiments make use of two real robot models (Pioneer P3DX and Robotnik SUMMIT-XL), and different tasks (reaching a target, avoiding collisions, and following another moving object). We recognize the need for further experiments, including using physical robots, and we mentioned it as part of our future work.

\bigskip

\textit{``The authors extended the previous paper by expanding related work, especially with respect to ROS (Robot Operating System) and adding two other experiments similar to the one they had already conducted.}

\textit{It's a good paper, but I'm worried that the extension is thin. As a rule of thumb, I expect at least 30\% more content in sections related to the proposal itself (thus, introduction, background, related work and conclusions shouldn't count in my humble opinion). As such, the two new experiments, very similar to the one already conducted, are not enough of an extension to me.''}

\smallskip
\underline{Response}: This extended version does not directly extends the framework architectural structure and behavior, but provides more details that were not present in the conference version. The paper is intended to present the the core of the framework, which should not grow with capabilities that could be targeting only specific scenarios or robot models. As such, the paper does not provide extension to the architecture of the framework. 

We envision the addition of further capability as extensions and libraries. Actually, we are launching the project as an open source initiative (the GitHub repository is reference in the paper) as a way to grow the framework. Still, the guiding principle would be to keep the core as simple as possible, which is basically the one presented in this paper. 

%LeoM: Acho que isso não precisa :)
%Regarding the other contributions, we would like to remark that literature reviews and papers reporting primary studies (controlled experiments, case studies, surveys, etc.) would be almost unfeasible to extend if the new contribution involves exclusively the proposal.  
%Therefore, such additional modifications should not be considered effortless.

Regarding the other contributions, we extended the related work discussing further related proposals material, in particular the robotics standards from the OMG, and specific work mentioned by another reviewer. These contributions position the proposal with respect to recent existing work. This involved the analysis of such related contribution, and the  understanding how they relate to our work.

The discussion of relationship with ROS is different, and we dedicated an entirely new section to it (Section 7). In fact, it is not just the discussion of related work. In that section we explain how our frameowrk, RoCS, could be implemented on top of ROS. 
To do this, we had to analyze the ROS structure, which required discussions with robot developers experienced with ROS. 
As such, this section should be considered part of our contribution.
 
Finally, we extended the paper with two new experiments, which are similar in the execution, but not in their implementation. 
The objective of the experiments is understanding the effort in adapting code to different robots and/or different application tasks.
The two additional experiments involved design, implementation (coding the robot models and behaviors, as well as the integration with the VRep simulator), testing, and analysis. We entirely reworked Section 6 for a better reporting the purpose and execution of the three experiments. We expect this can clarify the effort of conducting them and their differences and similarities.

Therefore, we believe that all these new contributions, as a whole, should not be considered only additional content, but should qualify as technical extension.

\bigskip

\textit{``One part of the proposal that could be further discussed is priorities. When explaining the Execute thread, you mention that the "behavior is simple: at each iteration it gets the next Action in the pipeline and executes it, by calling the act method." However, in the next paragraph you talk about Actions having high-priority. Explain, thus, how the Execute component defines which Action is next. Further, shouldn't there be a priority attribute in Action in Figure 3?''}

\smallskip

\underline{Response}: Thanks for the comment. We clarified the priority queue in the Section 5.1 with the following paragraph:
``Communication between the \texttt{Plan} and the \texttt{Execute} occurs through a queue, implemented by the \texttt{Pipeline} class. This queue contains a list of actions that are meant to be executed, as objects of type \texttt{Action}. Because the possible actions depend on the kind of robot and scenario at hand, the \texttt{Action} class is abstract and is meant to be extended by users of the framework. The queue is actually a \emph{priority queue}, to allow the reactive behavior to override the decisions taken by the Plan block (deliberative behavior), by submitting actions with a higher priority. 
The priority is assigned to actions when they are inserted in the queue. Two levels of priority are supported: \emph{high} and \emph{normal}. The action that is selected for execution is thus the first action with \emph{high} priority in the queue or, if no high priority actions exist, the first action with \emph{normal} priority. When the reactive action route is active, the pipeline is cleared and then re-plan.''

\bigskip

\textit{``Another point in which more content could be added is Section 6.2: the results of EXP02 are not discussed. Why?''}

\smallskip

\underline{Response}: As mentioned above, we reworked all Section 6. Now, the EXP02 is better explained in Section 6. Robot configurations are described in 6.1, Questions are in presented in 6.2, and Results are presented in 6.4. 

\bigskip

\textit{``Minor issues:}

\textit{* The last paragraph of the introduction doesn't mention Section 7;}

\textit{* "batteries have a short autonomy" -> "batteries have short autonomy"}

\textit{* "guide the user in structuring and reuse its code." -> "structuring and reusing"}

\textit{* "a complex hierarchy impose" -> "imposes"}

\textit{* "between robots components" -> "between the robots' components" or "between robot components"}

\textit{* "defines a examples interface" -> "defines an example interface" or "defines extample interfaces"}

\textit{* In Section 5.2, "ReactiveBehavior" is exceeding the margins. If using LaTeX, replace the word with Re\-ac\-tive\-Be\-hav\-ior so LaTeX inserts an hyphen at some of these breaking points.}

\textit{* "Figure 5 depicts the resulting trajectory performed by the robot" -> do you mean "Figure 5b"?}

\textit{* "three correspondly Analyzers" -> "three corresponding Analyzers"}

\textit{* "Figures 8 (b) and (c) depicts" -> "depict"}

\textit{* "sets of related files, in which includes" -> "sets of related files, which include"}

\textit{* In Fig. 9, class SensorNode, attribute rosSensorMsg doesn't follow UML syntax. Should be "- rosSensorMsg : Value", right?}
\underline{Response}: Yes, corrected. 

\textit{* Finally, why are Fig. 8 and Fig. 9 at the end of the paper? Having them in the next page after they are first mentioned makes for better reading.''}

\smallskip

\underline{Response}: All fixed.

---------------------------------------------------------------------------------------------------

\textbf{Reviewer C}

\textit{``In this paper, the authors develop the RoCS (Robotics and Cognitive Systems) framework for autonomous robots based on the MAPE-K architecture.}

\textit{The paper is well written and well structure, and the contributions are definitely interesting and in the scope of the journal.''}

\smallskip

\underline{Response}: Thanks for acknowledging our contribution.

\bigskip

\textit{``However, a few things should be improved before publication:}

\textit{Authors should discuss in Section 4.1. the differences of their contributions with the work developed by Prof. Glesner, e.g.,: Klös, V., Göthel, T. and Glesner, S., 2015, August. Adaptive knowledge bases in self-adaptive system design. In 2015 41st Euromicro Conference on Software Engineering and Advanced Applications (pp. 472-478).''}

\smallskip

\underline{Response}: We now discuss the difference to this work in the Section 3, in the following paragraph: ``Similarly to our proposal, the authors of [Klos15] also propose an extension of the MAPE-K architecture; the focus is however different. The work in [Klos15] focuses on self-adaptive systems, like the original MAPE-K proposal, and proposes extensions to supporting the adaptation of the adaptation algorithm itself. A proof of concept based on the Communicating Sequential Processes (CSP) process calculus is provided. In this work, we focus instead on the domain of autonomous robots (which are not necessarily \emph{self}-adaptive), and we propose a general framework to support the development of software for such platforms. Specific algorithms for (self-)adaptation are out of the scope of this paper.''

\bigskip

\textit{``Figure 2 should be bigger. The text is very difficult to read in a print version.''}

\smallskip

\underline{Response}: We enlarged the components displayed in the Figure.

\bigskip

\textit{``In the first paragraph of Section 6, it is not clear if you authors use three or 2 robots. It seems they use 3: 2 following the fw and 2 not? Can you clarify?''}

\smallskip

\underline{Response}: Thanks for notice. Actually, we used three robots. We reworked the entire Section 6 for a better reporting of the experiments. We also added the trajectory performed by the robot in EXP2 and links for videos of EXP2 and EXP3.

\bigskip

\textit{``Section 6 explains the design of 3 experiments, but the results and validation purpose of these experiments are not clear. What is what is being tested: the feasibility, the easiness of using the framework? I would suggest the authors to clarify first the evaluation criteria and explain the results after the design of each experiment. It is also important to make explicit who developed these experiments. The authors claim that the contributions of the paper can assist students or novices in robot development, are the experiments proving this? Are they developed by  students or novices? If not, the authors should indicate that further experiments are needed to validate this statement. It would also facilitate the reading of the experiments if all would follow the same explanation structure.''}

\vspace{0.5cm}

\underline{Response}: As mentioned in the previous comment, we reworked this section. Now, the configurations of robots are in Section 6.1, the objective and questions are in Section 6.2, the results are in Section 6.3/6.4/6.5 for each experiment. 
The authors designed and analyzed the experiments together. However, two of the authors are experts in software and systems architecture with no previous experience in the robotics domain (BBNF and LM). One of the authors (LR) has some experience in developing robots and using the  VRep simulator. He implemented the instantiated robots (structure and behavior) for the VRep environemnt. The other two authors are experienced with robot architectures (GLGD) and ROS environment (GCL). Only the last author (ELC) has several years of experience on robot design, development (software and hardware), simulation, and research. So, yes, the implementation is possible with robot developers with little previous experience. All the experiments were evaluated/analyzed by, at least, three researchers.

\bigskip

\textit{``Add code of experiment in label of Table 1.''}

\smallskip
\underline{Response}: Fixed.

\bigskip

\textit{``Just before Fig. 5 it is said: the following extension points. Are they next explained? or are they just shown in Figure 6? If the later, "following" should be deleted.''}

\smallskip

\underline{Response}:  Fixed.

\bigskip

\textit{``Fig. 8 and 9 should be moved close to the sections where they are referred to.''}

\smallskip

\underline{Response}: Thanks, we moved them.


\end{document}
