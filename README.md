* Current Version [[PDF](https://gitlab.com/montex/jserd2019/-/jobs/artifacts/master/file/main.pdf?job=pdf)]
* Differences from CIBSE2019 [[PDF](https://gitlab.com/montex/jserd2019/-/jobs/artifacts/master/file/main_diff_CIBSE2019-cameraready.pdf?job=diff-baseline)]
* Differences from JSERD Submitted [[PDF](https://gitlab.com/montex/jserd2019/-/jobs/artifacts/master/file/main_diff_JSERD-submitted.pdf?job=diff-baseline)]
